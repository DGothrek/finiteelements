import triangle

import numpy as np

import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt


def meshEdges(m):
    """
        Input: un maillage m

        Outputs: 'edge' est un tableau avec la liste de toutes les arêtes du maillage
                 'edge_markers' renvoie le label des arêtes avec 
                        0 pour une arête intérieure qui n'est pas un segment fixé par le maillage
                        i>0 où i est le label du bord sur lequel se trouve l'arête
                 'ElementEdges' est un tableau contenant, pour chaque triangle, la liste de ses arêtes

       Note : Pour avoir les labels des arêtes il faut garder le champ 'segments' dans le maillage
              donc il faut mettre l'option 'p' lorsqu'on construit le maillage m
              m = triangle.triangulate(A,'p')

    """

    tri = m['triangles']
    nb_tri, _ = tri.shape

    e01 = np.array([tri[:, 0], tri[:, 1]]).T
    e01 = np.array([np.amin(e01, axis=1), np.amax(e01, axis=1)]).T

    e12 = np.array([tri[:, 1], tri[:, 2]]).T
    e12 = np.array([np.amin(e12, axis=1), np.amax(e12, axis=1)]).T

    e02 = np.array([tri[:, 0], tri[:, 2]]).T
    e02 = np.array([np.amin(e02, axis=1), np.amax(e02, axis=1)]).T

    etot = np.concatenate((e01, e12, e02))

    edge, indices = np.unique(etot, axis=0, return_inverse=True)

    ElementEdges = np.array(
        [indices[nb_tri:2*nb_tri], indices[2*nb_tri:], indices[:nb_tri]]).T

    if 'segments' not in m:
        raise ValueError(
            "Attention : il faut ajouter l'option 'p' dans la commande triangle.triangulate")

    seg = m['segments']
    seg = np.array([np.amin(seg, axis=1), np.amax(seg, axis=1)]).T

    segedg = np.concatenate((edge, seg))
    _, ind_lab = np.unique(segedg, axis=0, return_inverse=True)

    ne = edge.shape[0]

    edge_markers = np.zeros((ne, 1), dtype='int')
    edge_markers[ind_lab[ne:]] = m['segment_markers']

    return edge, edge_markers, ElementEdges

def plot_mesh(mesh, options=''):
    """
        Input: - un maillage mesh
               - des options sous la forme d'une str contenant :
                   - 'v' Affiche les numeros des sommets
                   - 'e' Affiche les numeros des aretes
                   - 't' Affiche les numeros des triangles
                   - 'V' Affiche les labels des sommets
                   - 'E' Affiche les labels des aretes
                   - 'g' Affiche la geometrie
        Output : Affiche la representation
    """
    eps=0.03
    x = mesh['vertices'][:,0]
    y = mesh['vertices'][:,1]
    labelsv = mesh['vertex_markers']
    triangles = mesh['triangles']
    plt.triplot(x,y,triangles)
    # Id sommets
    if options.find('v')>=0:
        for i in range(len(x)):
            xc = x[i]
            yc = y[i]
            plt.text(xc+eps, yc+eps, i)
    # Id triangles
    if options.find('t')>=0:
        for i in range(len(triangles)):
            s1, s2, s3 = triangles[i]
            xc = (x[s1]+x[s2]+x[s3])/3
            yc = (y[s1]+y[s2]+y[s3])/3
            plt.text(xc, yc, i,color='r')
    if options.find('V')>=0:
        for i in range(len(labelsv)):
            xc = x[i]
            yc = y[i]
            plt.text(xc+eps, yc+eps, labelsv[i][0])
    if options.find('e')>=0:
        edge, edge_markers, _ = meshEdges(mesh)
        for i in range(len(edge)):
            xc = (x[edge[i][0]]+x[edge[i][1]])/2
            yc = (y[edge[i][0]]+y[edge[i][1]])/2
            plt.text(xc, yc, i)
    if options.find('E')>=0:
        edge, edge_markers, _ = meshEdges(mesh)
        for i in range(len(edge)):
            xc = (x[edge[i][0]]+x[edge[i][1]])/2
            yc = (y[edge[i][0]]+y[edge[i][1]])/2
            plt.text(xc, yc, edge_markers[i][0])
    if options.find('g')>=0 and 'segments' in mesh:
        segments = mesh['segments']
        for segment in segments:
            plt.plot([x[segment[0]],x[segment[1]]],[y[segment[0]],y[segment[1]]], color='r')
    plt.axis('off')
    plt.axis('equal')
    # plt.show()
    return



def first_domain():
    """
        Test of the triangle library.
    """
    D = {'nom': 'first_domain', 'vertices':
         [
                [0.0, 0.0],
                [3.0, 0.0],
                [3.0, 1.5],
                [0.0, 1.5]
         ],
         'segments': [
             [1, 2],
             [2, 3],
             [3, 4],
             [4, 1]
         ]
         }

#    m=triangle.triangulate(D)

    aire = 0.2
    m2 = triangle.triangulate(D, 'Da{}'.format(aire))

    n = len(m2['triangles'])
    tris = m2['triangles']
    x = m2['vertices'][:,0]
    area = [0.2*((x[tris[i][0]]+x[tris[i][1]]+x[tris[i][2]])/3)**2 for i in range(n)]
    m2['triangle_max_area']=area
    m3 = triangle.triangulate(m2,'Dra')
    plot_mesh(m3, '')
    
    return


#first_domain()

def geom_disc(n):
    """
        Input : number of segments composing the circle of radius 1/2 and center 0
        Output : Domain corresponding to the circle inside the square [-1,1]^2
    """
    verticesc = [[np.cos(2*i*np.pi/n)/2,np.sin(2*i*np.pi/n)/2] for i in range(n)]
    vertices = verticesc + [[-1., -1.], [-1., 1.], [1., 1.],[1., -1.]]
    segments = [[i,i+1]for i in range(n-1)]+[[n-1,0]]+[[n+1,n]]+[[n+1,n+2]]+[[n+2,n+3]]+[[n,n+3]]
    markers=[1]*(n+4)
    regions = [[0.,0.,0,0.05],[1.,1.,0,0.01]]
    D = {"vertices":vertices, "segments":segments, "vertex_markers":markers, 'regions':regions}
    return D

def second_domain():
    D = geom_disc(12)
    aire = 0.1
    m = triangle.triangulate(D, 'Dpa')
    print(m)
    plot_mesh(m,'g')

# second_domain()