import numpy as np
import triangle
import matplotlib.pyplot as plt
import TP2
from TP3 import formInt1D, buildInteg1D
import TP4
import scipy.sparse as sp


def Lagrange1D(x, ordre):
    """
    Returns phi, dxphi, the values of base functions to the point list x
    for the given ordre (ordre=0 or 1)
    """
    phi = []
    dxphi = []
    n = len(x)
    if ordre == 0:
        phi = [np.ones(n)]
        dxphi = [np.zeros(n)]
    if ordre == 1:
        phi = [1-x, x]
        dxphi = [-np.ones(n), np.ones(n)]
    if ordre == 2:
        phi = [(x-0.5)*(x-1)*2, -x*(x-1)*4, x*(x-0.5)*2]
        dxphi = []
    return phi, dxphi


def FEspace(mesh, typeFE='Lagrange', ordre=1, formule=1):
    ef = {}
    vertices = mesh['vertices']
    edges, edge_markers, _ = TP2.meshEdges(mesh)
    ef2D = TP4.FEspace(mesh, ordre=ordre, formule=formule)
    borders, _ = np.nonzero(edge_markers)
    Seg = formInt1D(formule)
    phi, _ = Lagrange1D(Seg[0], ordre)
    npoints = len(Seg[0])
    nbase = 1
    border_edges = edges[borders]
    ddl, numDdl = [], []
    if ordre == 0:
        numDdl = np.arange(0, len(borders), dtype=int)
        ddl = (vertices[border_edges[:, 0]] + vertices[border_edges[:, 1]])/2
    if ordre == 1:
        nbase = 2
        bordervertices,_ = np.where(mesh['vertex_markers']==1)
        inv = np.zeros(len(vertices), dtype=int)
        for i in range(len(bordervertices)):
            inv[bordervertices[i]] = i
        nedges = len(border_edges)
        # print(border_edges)

        ddl = vertices[bordervertices]
        # ddl, indices = np.unique(ddl, return_inverse=True, axis=0)
        numDdl = np.zeros((nedges, nbase), dtype=int)
        numDdl[:, 0] = inv[border_edges[:,0]]
        numDdl[:, 1] = inv[border_edges[:,1]]

    if ordre == 2:
        nbase = 3
        nedges = len(border_edges)
        # print(border_edges)
        bordervertices,_ = np.where(mesh['vertex_markers']==1)
        inv = np.zeros(len(vertices), dtype=int)
        for i in range(len(bordervertices)):
            inv[bordervertices[i]] = i
        nedges = len(border_edges)

        ddl = np.zeros((len(bordervertices)+nedges, 2))
        ddl[:len(bordervertices)] = vertices[bordervertices]
        ddl[len(bordervertices):] = (vertices[border_edges[:, 0]] +
                                vertices[border_edges[:, 1]])/2
        numDdl = np.zeros((nedges, nbase), dtype=int)
        numDdl[:, [0, 2]] = inv[border_edges[:]]
        numDdl[:, 1] = np.arange(len(bordervertices), len(bordervertices)+nedges)
        # print(numDdl)
        # print(ddl)

    ef['ddl'] = ddl
    ef['numDdl'] = numDdl

    M = nedges*npoints
    N = len(ef['ddl'])

    # Build u

    i = np.zeros(nedges*npoints*nbase)
    j = np.zeros(nedges*npoints*nbase)
    a = np.zeros(nedges*npoints*nbase)
    for k in range(nbase):
        for l in range(npoints):
            i[k*npoints+l::npoints*nbase] = [m*npoints+l for m in range(nedges)]
            j[k*npoints+l::npoints*nbase] = ef['numDdl'][:, k]
            a[k*npoints+l::npoints*nbase] = [phi[k][l]] * nedges
    ef['u'] = sp.csc_matrix((a, (i, j)), shape=(M, N))

    return ef

def evaluate_f(ddl):
    # returns x
    x = ddl[:,0]
    return(np.exp(x))

def test():
    # Compute pi
    nv = 100
    vertices = [[np.cos(2*i*np.pi/nv), np.sin(2*i*np.pi/nv)]
                for i in range(nv)]
    segments = [[i, i+1]for i in range(nv-1)] + [[nv-1,0]]
    # print(segments)
    D = {"vertices": vertices, "segments": segments}
    aire = 0.01
    formule=3
    m = triangle.triangulate(D, 'Dpa{}'.format(aire))
    ef = FEspace(m, typeFE='Lagrange', ordre=1, formule=formule)
    fh = evaluate_f(ef['ddl'])
    fu = ef['u'].dot(fh)
    # print(fu)
    integ = buildInteg1D(m, formule)
    res = np.sum(integ['w']*fu)
    print(res/2)
    
    TP2.plot_mesh(m)

def test_exp():
    vertices = [[0, 0], [0, 1], [1, 1], [1, 0]]
    segments = [[0, 1], [1, 2], [2, 3], [3, 0]]
    D = {"vertices": vertices, "segments": segments}
    aire = 0.005
    formule = 3
    m = triangle.triangulate(D, 'Dpa{}'.format(aire))
    ef = FEspace(m, typeFE='Lagrange', ordre=2, formule=formule)
    fh = evaluate_f(ef['ddl'])
    fu = ef['u'].dot(fh)
    integ = buildInteg1D(m, formule)
    exa = np.sin(integ['x']*np.pi)
    res = np.sum(integ['w']*fu)
    
    # print(integ['x']*np.pi)
    # print(ef['ddl'][:,0]*np.pi)
    # print(exa-fu)
    # print(fu)
    print(3*np.exp(1) -1)
    print(res)
    plt.plot(ef['ddl'][:,0], ef['ddl'][:,1], 'go')
    plt.plot(integ['x'], integ['y'], 'r+')
    # for i in range(len(integ['x'])):
    #     plt.text(integ['x'][i]+0.02, integ['y'][i]+0.02, str(integ['w'][i])[:4], color= 'g')
    TP2.plot_mesh(m)

def test_tot():
    """Handles the passage matrix to full space."""
    vertices = [[0, 0], [0, 1], [1, 1], [1, 0]]
    segments = [[0, 1], [1, 2], [2, 3], [3, 0]]
    D = {"vertices": vertices, "segments": segments}
    aire = 0.0005
    formule = 3
    m = triangle.triangulate(D, 'Dpa{}'.format(aire))
    ef1D = FEspace(m, ordre=2, formule=formule)
    ef2D = TP4.FEspace(m,ordre=2,formule=formule)
    plt.plot(ef2D['ddl'][:,0], ef2D['ddl'][:,1], 'go')
    plt.plot(ef1D['ddl'][:,0], ef1D['ddl'][:,1], 'r+')
    npts1D, npts2D = len(ef1D['ddl']),len(ef2D['ddl'])
    indices = ef2D['ddl']
    i = np.arange(npts1D)
    j = ef2D['1Dindices']
    a = np.ones(npts1D)
    Q = sp.csc_matrix((a, (i, j)), shape=(npts1D, npts2D))

    fh = evaluate_f(ef2D['ddl'])
    fu = ef1D['u']*Q*fh
    integ = buildInteg1D(m, formule)
    exa = np.sin(integ['x']*np.pi)
    res = np.sum(integ['w']*fu)
    print(3*np.exp(1) -1)
    print(res)
    TP2.plot_mesh(m)

# test_exp()
# test_tot()