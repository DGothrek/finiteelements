import triangle
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as lg
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import TP2
import TP3
import TP4
import TP5
from TP6 import plot_sol_3d


def plot_sol_3d_stokes(x, y, vx, vy, xp, yp, p):
    """
    Wrapper to plot effectively a solution
    of Stokes equation (v & p) in 3D.
    """
    fig = plt.figure()
    ax = fig.add_subplot(1, 3, 1, projection='3d')

    ax.plot_trisurf(x, y, vx, cmap=cm.Spectral, linewidth=0, antialiased=False)

    ax = fig.add_subplot(1, 3, 2, projection='3d')

    ax.plot_trisurf(x, y, vy, cmap=cm.Spectral, linewidth=0, antialiased=False)

    ax = fig.add_subplot(1, 3, 3, projection='3d')

    ax.plot_trisurf(xp, yp, p, cmap=cm.Spectral, linewidth=0, antialiased=False)

    plt.show()


def stokes_solver():
    """Solves the stokes equation for simple limit conditions."""

    # Defines constants for problem.
    vertices = [[0, 0], [1, 0], [1, 1], [0, 1]]
    segments = [[0, 1], [1, 2], [2, 3], [3, 0]]
    segment_markers = [1, 2, 3, 2]
    formule = 4
    aire = 0.002
    ordrev = 2
    ordrep = 1
    eps = 1e-1

    domain = {"vertices": vertices, "segments": segments,
              "segment_markers": segment_markers}
    mesh = triangle.triangulate(domain, 'Dpa{}'.format(aire))

    # Fix vertex label
    mesh['vertex_markers'][3] = 2

    ef1 = TP4.FEspace(mesh, typeFE='Lagrange', ordre=ordrev, formule=formule)
    ef2 = TP4.FEspace(mesh, typeFE='Lagrange', ordre=ordrep, formule=formule)
    integ = TP3.buildInteg(mesh, formule)

    u1, dxu1, dyu1 = ef1['u'], ef1['dxu'], ef1['dyu']
    u2 = ef2['u']

    n_v = len(ef1['ddl'])
    n_p = len(ef2['ddl'])

    # Creating matrix P to mask unwanted ddl

    indices_to_keep = [0, 3]
    # print(ef1['lblDdl'])
    mask = np.isin(ef1['lblDdl'], indices_to_keep)
    i = np.where(mask)[0]
    j = np.arange(len(i))
    a = np.ones(len(i), dtype=float)
    P = sp.csc_matrix((a, (i, j)), shape=(len(mask), len(i)))

    # Computing the matrix A in equation AU=F
    D = sp.diags([integ['w']], [0])

    A11 = P.T*((2.*(dxu1.T*D*dxu1)) + (dyu1.T*D*dyu1))*P
    A21 = P.T*(dxu1.T*D*dyu1)*P
    A12 = P.T*(dyu1.T*D*dxu1)*P
    A22 = P.T*((dxu1.T*D*dxu1) + (2.*(dyu1.T*D*dyu1)))*P
    A31 = -(u2.T*D*dxu1)*P
    A32 = -(u2.T*D*dyu1)*P
    A13 = -P.T*dxu1.T*D*u2
    A23 = -P.T*dyu1.T*D*u2
    A33 = eps*u2.T*D*u2

    # A11 = P.T*(((dxu1.T*D*dxu1)) + (dyu1.T*D*dyu1))*P
    # A21 = None
    # A12 = None
    # A22 = P.T*((dxu1.T*D*dxu1) + ((dyu1.T*D*dyu1)))*P
    # A31 = -(u2.T*D*dxu1)*P
    # A32 = -(u2.T*D*dyu1)*P
    # A13 = -P.T*dxu1.T*D*u2
    # A23 = -P.T*dyu1.T*D*u2
    # A33 = eps*u2.T*D*u2

    buildA = [[A11, A12, A13],
              [A21, A22, A23],
              [A31, A32, A33]]

    A = sp.bmat(buildA, format='csc')

    # Computing matrix F
    xv = ef1['ddl'][:, 0]
    yv = ef1['ddl'][:, 1]
    xp = ef2['ddl'][:, 0]
    yp = ef2['ddl'][:, 1]

    xstar = np.zeros((2*len(i)+n_p), dtype=float)
    xstar[len(i):2*len(i)] = (xv*(1-xv))*P

    g = 10
    fy = (( - g)*np.ones((n_v), dtype=float))

    F = np.zeros((2*len(i)+n_p), dtype=float)
    F[len(i):2*len(i)] = P.T*u1.T*D*u1*fy
    # F = F - (A*xstar)

    # Solving Equation
    print("Solving...")
    U = lg.spsolve(A, F)
    print("Solving Done !")

    # Displaying solution
    ustary = xv*(1-xv)
    vx = (P*U[:len(i)])
    vy = (P*U[len(i):2*len(i)])+ustary
    p = U[2*len(i):]

    # plot_sol_3d(xv, yv, vx)
    # plot_sol_3d(xv, yv, vy)
    # plot_sol_3d(xp, yp, p)
    # plt.show()
    plot_sol_3d_stokes(xv, yv, vx, vy, xp, yp, p)

    return


def stokes_evolution_solver():
    """Simulates the evolution of the fluid"""

    dt = 1e-1
    n_iter = 30

    vertices = [[0, 0], [1, 0], [1, 1], [0, 1]]
    segments = [[0, 1], [1, 2], [2, 3], [3, 0]]
    segment_markers = [1, 2, 3, 2]
    vertex_markers = [1, 1, 2, 2]
    formule = 4
    aire = 1e-3
    ordrev = 2
    ordrep = 1
    eps = 1e-4

    domain = {"vertices": vertices, "segments": segments,
              "segment_markers": segment_markers, 'vertex_markers': vertex_markers}
    mesh = triangle.triangulate(domain, 'Dpa{}'.format(aire))

    # Fix vertex label
    mesh['vertex_markers'][3] = 2

    for step in range(n_iter):
        ef1 = TP4.FEspace(mesh, typeFE='Lagrange',
                          ordre=ordrev, formule=formule)
        ef2 = TP4.FEspace(mesh, typeFE='Lagrange',
                          ordre=ordrep, formule=formule)
        integ = TP3.buildInteg(mesh, formule)

        u1, dxu1, dyu1 = ef1['u'], ef1['dxu'], ef1['dyu']
        u2 = ef2['u']

        n_v = len(ef1['ddl'])
        n_p = len(ef2['ddl'])

        # Creating matrix P to mask unwanted ddl

        indices_to_keep = [0, 3]
        # print(ef1['lblDdl'])
        mask = np.isin(ef1['lblDdl'], indices_to_keep)
        i = np.where(mask)[0]
        j = np.arange(len(i))
        a = np.ones(len(i), dtype=float)
        P = sp.csc_matrix((a, (i, j)), shape=(len(mask), len(i)))

        # Computing the matrix A in equation AU=F
        D = sp.diags([integ['w']], [0])

        A11 = P.T*((2.*(dxu1.T*D*dxu1)) + (dyu1.T*D*dyu1))*P
        A21 = P.T*(dxu1.T*D*dyu1)*P
        A12 = P.T*(dyu1.T*D*dxu1)*P
        A22 = P.T*((dxu1.T*D*dxu1) + (2.*(dyu1.T*D*dyu1)))*P
        A31 = -(u2.T*D*dxu1)*P
        A32 = -(u2.T*D*dyu1)*P
        A13 = -P.T*dxu1.T*D*u2
        A23 = -P.T*dyu1.T*D*u2
        A33 = eps*u2.T*D*u2

        A11 = P.T*(((dxu1.T*D*dxu1)) + (dyu1.T*D*dyu1))*P
        A21 = None
        A12 = None
        A22 = P.T*((dxu1.T*D*dxu1) + ((dyu1.T*D*dyu1)))*P
        A31 = -(u2.T*D*dxu1)*P
        A32 = -(u2.T*D*dyu1)*P
        A13 = -P.T*dxu1.T*D*u2
        A23 = -P.T*dyu1.T*D*u2
        A33 = eps*u2.T*D*u2

        buildA = [[A11, A12, A13],
                  [A21, A22, A23],
                  [A31, A32, A33]]

        A = sp.bmat(buildA, format='csc')

        # Computing matrix F
        xv = ef1['ddl'][:, 0]
        yv = ef1['ddl'][:, 1]
        xp = ef2['ddl'][:, 0]
        yp = ef2['ddl'][:, 1]

        g = 10
        fy = ((-2 - g)*np.ones((n_v), dtype=float))

        F = np.zeros((2*len(i)+n_p), dtype=float)
        F[len(i):2*len(i)] = P.T*u1.T*D*u1*fy

        # Solving Equation
        print("Solving...")
        U = lg.spsolve(A, F)
        print("Solving Done !")

        # Displaying solution
        ustary = xv*(1-xv)
        vx = (P*U[:len(i)])
        vy = (P*U[len(i):2*len(i)])+ustary
        p = U[2*len(i):]

        # Move mesh
        mesh['vertices'][:, 0] += dt*vx[:len(mesh['vertices'])]
        mesh['vertices'][:, 1] += dt*vy[:len(mesh['vertices'])]

        # TP2.plot_mesh(mesh, options='v')
        # plt.show()

        # Need to retriangulate
        indices, _ = np.where(mesh['vertex_markers'] != 0)
        vertices = mesh['vertices'][indices]
        labels = mesh['vertex_markers'][indices]
        edges, edge_markers, _ = TP2.meshEdges(mesh)
        indices_edges, _ = np.where(edge_markers != 0)
        edges_border = edges[indices_edges, :]
        correct_vertices = np.cumsum(
            1*np.isin(mesh['vertex_markers'], 0, invert=True))-1
        edges = np.zeros(np.shape(edges_border))
        edges[:, 0] = correct_vertices[edges_border[:, 0]]
        edges[:, 1] = correct_vertices[edges_border[:, 1]]
        domain = {"vertices": vertices,
                  "vertex_markers": labels, 'segments': edges, 'segment_markers': edge_markers[indices_edges]}
        mesh = triangle.triangulate(domain, 'Dpa{}'.format(aire))

        # Display evolution
        TP2.plot_mesh(mesh, options='V')
        plt.pause(0.1)
        plt.clf()
    plt.show()


stokes_evolution_solver()
