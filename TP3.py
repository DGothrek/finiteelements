import numpy as np
import warnings
import triangle
import matplotlib.pyplot as plt
import TP2


def clean_points(tab):
    return [[x[0] for x in tab[:len(tab)-1]], [x[1] for x in tab[:len(tab)-1]], tab[-1]]


def formInt2D(numero):
    x1 = -9/32
    x2 = 25/96
    x3 = 1/60
    x4 = 3/20
    a = (6 + np.sqrt(15))/21
    b = (6 - np.sqrt(15))/21
    A = (155+np.sqrt(15))/2400
    B = (155-np.sqrt(15))/2400
    switch = {
        1: [[1/3, 1/3], [1/2]],
        2: [[1/2, 1/2], [1/2, 0], [0, 1/2], [1/6, 1/6, 1/6]],
        3: [[1/6, 1/6], [1/6, 2/3], [2/3, 1/6], [1/6, 1/6, 1/6]],
        4: [[1/3, 1/3], [1/5, 1/5], [3/5, 1/5], [1/5, 3/5], [x1, x2, x2, x2]],
        5: [[1/2, 1/2], [1/2, 0], [0, 1/2], [1/6, 1/6], [1/6, 2/3], [2/3, 1/6], [x3, x3, x3, x4, x4, x4]],
        6: [[1/3, 1/3], [a, a], [1-(2*a), a], [a, 1-(2*a)], [b, b], [1-(2*b), b], [b, 1-(2*b)], [9/80, A, A, A, B, B, B]]
    }
    if not(numero in switch):
        warnings.warn("Wrong parameter used. Now used with 2.", Warning)
        return np.array(clean_points(switch[2]), dtype=float)
    else:
        return np.array(clean_points(switch[numero]), dtype=float)


def compute_int2d(f, numero):
    T = formInt2D(numero)
    res = 0
    omega = T[len(T)-1]
    for i in range(len(T)-1):
        res += omega[i]*f(T[i])
    return res


def test_int():
    def f1(x):
        return x[0]

    def f2(x):
        return x[1]

    def f3(x):
        return x[0]*x[0]

    def f4(x):
        return x[1]*x[1]

    def f5(x):
        return x[0]*x[1]
    for numero in range(1, 7):
        print("*******************\nTest for number {}\n".format(numero))
        integral = compute_int2d(f1, numero)
        print("Computed integral equals {} while supposed result is 0.1666\n".format(
            integral))
        integral = compute_int2d(f2, numero)
        print("Computed integral equals {} while supposed result is 0.1666\n".format(
            integral))
        integral = compute_int2d(f3, numero)
        print("Computed integral equals {} while supposed result is 0.0833\n".format(
            integral))
        integral = compute_int2d(f4, numero)
        print("Computed integral equals {} while supposed result is 0.0833\n".format(
            integral))
        integral = compute_int2d(f5, numero)
        print("Computed integral equals {} while supposed result is 0.0417\n".format(
            integral))

# test_int()


def formInt1D(numero):
    switch = {
        1: [[1/2], [1]],
        2: [[1/2-(1/(2*np.sqrt(3))), 1/2+(1/(2*np.sqrt(3)))], [1/2, 1/2]],
        3: [[(1-np.sqrt(3/5))/2, 1/2, (1+np.sqrt(3/5))/2], [5/18, 8/18, 5/18]]
    }
    if not(numero in switch):
        warnings.warn("Wrong parameter used. Now used with 2.", Warning)
        return np.array(switch[2], dtype=float)
    else:
        return np.array(switch[numero], dtype=float)


def buildInteg(mesh, numero):
    integ = {}
    xa = mesh['vertices'][mesh['triangles'][:, 0], 0]
    xb = mesh['vertices'][mesh['triangles'][:, 1], 0]
    xc = mesh['vertices'][mesh['triangles'][:, 2], 0]
    ya = mesh['vertices'][mesh['triangles'][:, 0], 1]
    yb = mesh['vertices'][mesh['triangles'][:, 1], 1]
    yc = mesh['vertices'][mesh['triangles'][:, 2], 1]
    xhat, yhat, omega = formInt2D(numero)
    N = len(xhat)
    w = np.zeros(N*len(xa))
    for i in range(N):
        w[i::N] = (abs((xb-xa)*(yc-ya)-((yb-ya)*(xc-xa))))*omega[i]

    x = np.zeros(N*len(xa))
    y = np.zeros(N*len(xa))
    for i in range(N):
        x[i::N] = (xb-xa)*xhat[i] + (xc-xa)*yhat[i] + xa
        y[i::N] = (yb-ya)*xhat[i] + (yc-ya)*yhat[i] + ya
    integ['w'] = w
    integ['x'] = x
    integ['y'] = y
    return integ


def buildInteg1D(mesh, formule):
    integ = {}
    vertices = mesh['vertices']
    edges, edge_markers, _ = TP2.meshEdges(mesh)
    borders, _ = np.nonzero(edge_markers)
    border_edges = edges[borders]
    xa = vertices[border_edges[:, 0], 0]
    xb = vertices[border_edges[:, 1], 0]
    ya = vertices[border_edges[:, 0], 1]
    yb = vertices[border_edges[:, 1], 1]
    xhat, omega = formInt1D(formule)
    N = len(xhat)
    w = np.zeros(N*len(xa))
    for i in range(N):
        w[i::N] = np.sqrt((xb-xa)*(xb-xa)+((yb-ya)*(yb-ya)))*omega[i]

    x = np.zeros(N*len(xa))
    y = np.zeros(N*len(xa))
    for i in range(N):
        x[i::N] = (xb-xa)*xhat[i] + xa
        y[i::N] = (yb-ya)*xhat[i] + ya
    integ['w'] = w
    integ['x'] = x
    integ['y'] = y
    return integ


def test_circle(num):
    nv = 100
    vertices = [[np.cos(2*i*np.pi/nv), np.sin(2*i*np.pi/nv)]
                for i in range(nv)]
    segments = [[i, i+1]for i in range(nv-1)]
    D = {"vertices": vertices, "segments": segments}
    aire = 0.01
    m = triangle.triangulate(D, 'Da{}'.format(aire))
    integ = buildInteg(m, num)
    print(integ['w'][:10])
    res = np.sum(integ['w'])
    print(res)
    plt.plot(integ['x'], integ['y'], '+r')
    TP2.plot_mesh(m)


# test_circle(4)
