import numpy as np
import triangle
import matplotlib.pyplot as plt
import TP2
from TP3 import formInt2D, buildInteg
import scipy.sparse as sp

# from time import time


def Lagrange2D(x, y, ordre):
    """
    Returns phi, dxphi, dyphi, the values of base functions to the point list x, y
    for the given ordre (ordre=0, 1 or 2)
    """
    phi = []
    dxphi = []
    dyphi = []
    n = len(x)
    if ordre == 0:
        phi = np.array([np.ones(n)])
        dxphi = np.array([np.zeros(n)])
        dyphi = np.array([np.zeros(n)])
    if ordre == 1:
        phi = np.array([-x-y+1, x, y])
        dxphi = np.array([-np.ones(n), np.ones(n), np.zeros(n)])
        dyphi = np.array([-np.ones(n), np.zeros(n), np.ones(n)])
    if ordre == 2:
        phi = np.array([2.*(x+y-0.5)*(x+y-1.), 2.*x*(x-0.5),
                        2.*y*(y-0.5), 4.*x*y, -4.*y*(x+y-1.), -4.*x*(x+y-1.)])
        dxphi = np.array([2.*(x+y-1)+2.*(x+y-0.5), 2.*x+2. *
                          (x-0.5), np.zeros(n), 4.*y, -4.*y, -4.*x-4.*(x+y-1.)])
        dyphi = np.array([2.*(x+y-1)+2.*(x+y-0.5), np.zeros(n),
                          2.*y+2.*(y-0.5), 4.*x, -4.*y-4.*(x+y-1.), -4.*x])

    return phi, dxphi, dyphi


def Lagrange1D(x, ordre):
    """
    Returns phi, dxphi, the values of base functions to the point list x
    for the given ordre (ordre=0 or 1)
    """
    phi = []
    dxphi = []
    n = len(x)
    if ordre == 0:
        phi = [np.ones(n)]
        dxphi = [np.zeros(n)]
    if ordre == 1:
        phi = [1-x, x]
        dxphi = [-np.ones(n), np.ones(n)]
    return phi, dxphi


def FEspace(mesh, typeFE='Lagrange', ordre=1, formule=1):
    ef = {}
    triangles = mesh['triangles']
    vertices = mesh['vertices']
    T = formInt2D(formule)
    phi, dxphi, dyphi = Lagrange2D(T[0], T[1], ordre)
    ntri = len(triangles)
    ninttri = len(T[0])
    nbase = 1
    if ordre == 0:
        ef['numDdl'] = np.arange(len(triangles))
        ef['ddl'] = (vertices[triangles[:, 0]] +
                     vertices[triangles[:, 1]] + vertices[triangles[:, 2]]) / 3
    if ordre == 1:
        ef['numDdl'] = mesh['triangles']
        ef['ddl'] = mesh['vertices']
        nbase = 3
        ef['1Dindices'], _ = np.where(mesh['vertex_markers'] == 1)
    if ordre == 2:
        nbase = 6
        edges, element_markers, elementEdges = TP2.meshEdges(mesh)
        numDdl = np.zeros((len(triangles), 6), dtype=int)
        ddl = np.zeros((len(vertices) + len(edges), 2))
        indicesVer, _ = np.where(mesh['vertex_markers'] == 1)
        indicesEdg, _ = np.where(element_markers == 1)
        indices = np.zeros(len(indicesVer)+len(indicesEdg), dtype=int)
        indices[:len(indicesVer)] = indicesVer
        indices[len(indicesVer):] = indicesEdg + len(indicesVer)
        ef['1Dindices'] = indices
        ddl[:len(vertices)] = vertices
        ddl[len(vertices):] = (vertices[edges[:, 0]] + vertices[edges[:, 1]])/2
        numDdl[:, :3] = triangles
        numDdl[:, 3:] = len(vertices) + elementEdges[:]

        ef['numDdl'] = numDdl
        ef['ddl'] = ddl

    # Creating an array lblDdl
    if ordre == 1:
        # The ddl are the vertices : we identify those on the edge by the vertex_markers array
        markers = mesh['vertex_markers']
        ef['lblDdl'] = markers
    if ordre == 2:
        _, edge_markers, _ = TP2.meshEdges(mesh)
        ef['lblDdl'] = np.concatenate(((mesh['vertex_markers'].T)[0], (edge_markers.T)[0]))
    M = ntri*ninttri
    N = len(ef['ddl'])

    # Build u

    i = np.zeros(ntri*ninttri*nbase)
    j = np.zeros(ntri*ninttri*nbase)
    a = np.zeros(ntri*ninttri*nbase)
    for k in range(nbase):
        for l in range(ninttri):
            i[k*ninttri+l::ninttri*nbase] = [m*ninttri+l for m in range(ntri)]
            j[k*ninttri+l::ninttri*nbase] = ef['numDdl'][:, k]
            a[k*ninttri+l::ninttri*nbase] = [phi[k][l]] * ntri
    ef['u'] = sp.csc_matrix((a, (i, j)), shape=(M, N))

    # Build dxu and dyu

    dxx = vertices[triangles[:, 1], 0] - vertices[triangles[:, 0], 0]
    dxy = vertices[triangles[:, 2], 0] - vertices[triangles[:, 0], 0]
    dyx = vertices[triangles[:, 1], 1] - vertices[triangles[:, 0], 1]
    dyy = vertices[triangles[:, 2], 1] - vertices[triangles[:, 0], 1]

    invdet = 1/((dxx*dyy) - (dxy*dyx))

    for k in range(nbase):
        for l in range(ninttri):
            i[k*ninttri+l::ninttri*nbase] = [m*ninttri+l for m in range(ntri)]
            j[k*ninttri+l::ninttri*nbase] = ef['numDdl'][:, k]
            a[k*ninttri+l::(ninttri *
                            nbase)] = (dxphi[k][l]*invdet*dyy)-(dyphi[k][l]*invdet*dyx)
    ef['dxu'] = sp.csc_matrix((a, (i, j)), shape=(M, N))
    for k in range(nbase):
        for l in range(ninttri):
            i[k*ninttri+l::ninttri*nbase] = [m*ninttri+l for m in range(ntri)]
            j[k*ninttri+l::ninttri*nbase] = ef['numDdl'][:, k]
            a[k*ninttri+l::(ninttri *
                            nbase)] = (dyphi[k][l]*invdet*dxx)-(dxphi[k][l]*invdet*dxy)
    ef['dyu'] = sp.csc_matrix((a, (i, j)), shape=(M, N))
    return ef


def evaluate_f(ddl):
    return(np.exp(ddl[:, 0]))


def test():
    vertices = [[0, 0], [0, 1], [1, 1], [1, 0]]
    segments = [[0, 1], [1, 2], [2, 3], [3, 0]]
    D = {"vertices": vertices, "segments": segments}
    aire = 0.5
    formule = 4
    m = triangle.triangulate(D, 'Dpa{}'.format(aire))
    ef = FEspace(m, typeFE='Lagrange', ordre=2, formule=formule)
    fh = evaluate_f(ef['ddl'])
    fu = ef['u'].dot(fh)
    print(ef['u'].toarray())
    integ = buildInteg(m, formule)
    res = np.sum(integ['w']*fu)
    print("Estimated integral: {}, real value: {}".format(res, np.exp(1)-1))
    print(ef['1Dindices'])
    return


def test_dist_h():
    vertices = [[0, 0], [0, 1], [1, 1], [1, 0]]
    segments = [[0, 1], [1, 2], [2, 3], [3, 0]]
    D = {"vertices": vertices, "segments": segments}
    formule = 5
    edge_length = np.array([0.1, 0.05, 0.02, 0.01])
    aires = edge_length * edge_length
    values = []
    for aire in aires:
        m = triangle.triangulate(D, 'Dpa{}'.format(aire))
        ef = FEspace(m, typeFE='Lagrange', ordre=2, formule=formule)
        fh = evaluate_f(ef['ddl'])
        # plt.plot(ef['ddl'][:, 0], ef['ddl'][:, 1], '+r')
        # TP2.plot_mesh(m)
        fu = ef['u'].dot(fh)
        integ = buildInteg(m, formule)
        exa = np.exp(integ['x'])
        res = np.sum(integ['w']*(fu-exa)*(fu-exa))
        print("Estimated integral: {}".format(res))
        values.append(abs(res))
    plt.plot(np.log10(edge_length), np.log10(np.sqrt(values)), '+r-')
    plt.show()


# test_dist_h()
# test()
