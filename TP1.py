# Author : Louis Raison

# TP1 1D


###############################################################################
# Computes a simple solution
###############################################################################

import numpy as np
from scipy import sparse as sp
import matplotlib.pyplot as plt

def calc_A(x):
    """Computes the symetric matrix A"""
    N = len(x)-2
    sub_diag = [-1/(x[i+1]-x[i]) for i in range(1,N)]
    diag = [1/(x[i]-x[i-1]) + 1/(x[i+1]-x[i]) for i in range(1,N+1)]
    A = sp.diags([sub_diag, diag, sub_diag], [-1,0,1])
    return A

def calc_F(x):
    """Computes F when f=1"""
    return np.array([(x[i+1]-x[i-1])/2 for i in range(1,len(x)-1)])

def solve_1(x):
    """Solves the equation for f=1 and compares to the real solution"""
    def f(t):
        return (t-t**2)/2
    y1 = (np.vectorize(f))(x)
    A = calc_A(x)
    F = calc_F(x)
    y2 = sp.linalg.spsolve(A,F)
    y2 = [0.]+list(y2)+[0.]
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.show()
    return y1, y2

def test_1():
    x = np.linspace(0.,1., 1000)
    y1, y2 = solve_1(x)
    print("Approximation homogene : "+str(max([abs(y1[i]-y2[i]) for i in range(len(y1))])))
    x2 = x**2
    # print(x2)
    y1, y2 = solve_1(x2)
    print("Approximation non homogene : "+str(max([abs(y1[i]-y2[i]) for i in range(len(y1))])))
    

#test_1()


###############################################################################
# Ameliorations
###############################################################################
    
def calc_UX(x):
    N = len(x)-2
    diag1 = [1/(x[i]-x[i-1])for i in range(1,N+1)]
    diag2 = [-1/(x[i+1]-x[i]) for i in range(1,N+1)]
    UX = sp.diags([diag1, diag2], [0,-1], shape=(N+1,N))
    return UX

def calc_D(x):
    N = len(x)-2
    return sp.diags([x[i]-x[i-1] for i in range(1,N+2)])

def test_2():
    """Correct ! A = UX’ D UX"""
    x = np.linspace(0.,1., 10)
    print(calc_F(x).toarray())
    UX = calc_UX(x)
    D = calc_D(x)
    print((UX.T*D*UX).toarray())
    return

def calc_U(x):
    N = len(x)-2
    diag1 = [1/2for i in range(1,N+1)]
    diag2 = [1/2 for i in range(1,N+1)]
    U = sp.diags([diag1, diag2], [0,-1], shape=(N+1,N))
    return U

def calc_Ftilde(f,x):
    N = len(x)-2
    return [(f(x[i+1])+f(x[i]))/2 for i in range(N+1)]

def solve_2(f,x):
    """Solves the equation for any f and compares to the real solution"""
    def u(t):
        return t*(1-t**2)/6
    y1 = (np.vectorize(u))(x)
    UX = calc_UX(x)
    D = calc_D(x)
    U = calc_U(x)
    F = U.T*D*calc_Ftilde(f, x)
    A = UX.T*D*UX
    y2 = sp.linalg.spsolve(A,F)
    y2 = [0.]+list(y2)+[0.]
    return y1, y2

def test_3():
    def f(t):
        return t
    x = np.linspace(0.,1., 1000)
    y1, y2 = solve_2(f, x)
    print("Approximation homogene : "+str(max([abs(y1[i]-y2[i]) for i in range(len(y1))])))
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.show()
    
    x2 = x**2
    y1, y2 = solve_2(f, x2)
    plt.plot(x2, y1)
    plt.plot(x2, y2)
    plt.show()
    print("Approximation non homogene : "+str(max([abs(y1[i]-y2[i]) for i in range(len(y1))])))
    
###############################################################################
# Plus difficile
###############################################################################


eps=0.01

def solve_3(x):
    """Solves the equation for any f and compares to the real solution"""
    def u(t):
        return t-((np.exp(t/eps)-1)/(np.exp(1/eps)-1))
    y1 = (np.vectorize(u))(x)
    UX = calc_UX(x)
    D = calc_D(x)
    U = calc_U(x)
    F = U.T*D*U*[1 for i in range(len(x)-2)]
    A = eps*UX.T*D*UX + U.T*D*UX
    y2 = sp.linalg.spsolve(A,F)
    y2 = [0.]+list(y2)+[0.]
    return y1, y2

def test_4():
    x = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
    y1, y2 = solve_3(x)
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.show()
    
    x2 = [0,0.75,0.875,0.9375,0.96875,0.984375,1]
    y1, y2 = solve_3(x2)
    plt.plot(x2, y1)
    plt.plot(x2, y2)
    plt.show()
    
test_4()