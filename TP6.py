import triangle
import numpy as np
import time
import scipy.sparse as sp
import scipy.sparse.linalg as lg

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import TP4
import TP3
import TP2

pi2 = 2*np.pi
vertices = [[0, 0], [0, pi2], [pi2, pi2], [pi2, 0]]
segments = [[0, 1], [1, 2], [2, 3], [3, 0]]
formule = 4

D = {"vertices": vertices, "segments": segments}


def plot_sol_3d(x, y, z):
    """
    Wrapper to plot effectively a solution in 3D
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.plot_trisurf(x, y, z, linewidth=0.2, antialiased=True)

def eval_f(pts):
    return np.sin(pts[:, 0])*np.sin(pts[:, 1])


def solve_dirichlet(m, order=1):
    """
    Solving the same equation but with dirichlet conditions.
    Input:
        - mesh m
        - order wanted for finite elements
    """
    # Construction des elements finis et de la formule d'integration
    top = time.time()
    print("Finite elements building")
    ef = TP4.FEspace(m, typeFE='Lagrange', ordre=order, formule=formule)
    print("Finite elements built in {}s".format(time.time()-top))
    integ = TP3.buildInteg(m, formule)

    # Find mesh edge points
    P = np.array([])
    if order == 1:
        # The ddl are the vertices : we identify those on the edge by the vertex_markers array
        markers = m['vertex_markers']
        N1 = len(m['vertices'])
        N2 = N1 - np.count_nonzero(markers)
        a, i, j = (np.zeros(N2), np.zeros(
            N2, dtype=int), np.zeros(N2, dtype=int))
        k = 0
        l = 0
        for pt in markers:
            if pt[0] == 0:
                a[k] = 1.
                i[k] = l
                j[k] = k
                k += 1
            l += 1
        P = sp.csc_matrix((a, (i, j)), shape=(N1, N2))
    if order == 2:
        _, edge_markers, _ = TP2.meshEdges(m)
        markers = m['vertex_markers']
        N1 = len(ef['ddl'])
        N2 = N1 - np.count_nonzero(markers) - np.count_nonzero(edge_markers)
        a, i, j = (np.zeros(N2), np.zeros(
            N2, dtype=int), np.zeros(N2, dtype=int))
        k = 0
        l = 0
        for pt in markers:
            if pt[0] == 0:
                a[k] = 1.
                i[k] = l
                j[k] = k
                k += 1
            l += 1
        for edge in edge_markers:
            if edge[0] == 0:
                a[k] = 1.
                i[k] = l
                j[k] = k
                k += 1
            l += 1
        P = sp.csc_matrix((a, (i, j)), shape=(N1, N2))

    x = ef['ddl'][:, 0]
    y = ef['ddl'][:, 1]

    # Computing matrix
    w = integ['w']
    D = sp.diags([w], [0])
    fh = 3*eval_f(ef['ddl'])
    u, dxu, dyu = ef['u'], ef['dxu'], ef['dyu']
    A = ((u.T)*D*u) + ((dxu.T)*D*dxu) + ((dyu.T)*D*dyu)
    Ap = P.T*A*P
    fp = P.T*(u.T)*D*u*fh + (P.T*(dxu.T)*D*dxu*x)+ ((P.T*(dyu.T)*D*dyu*x))

    # Solving Equation
    print("Solving...")
    sol = P*lg.spsolve(Ap, fp) + x
    print("Solving Done !")
    plot_sol_3d(x, y, sol)

    vu = eval_f(np.array([x, y]).T) + x
    diff = sol-vu
    errorL2 = np.sqrt(np.sum(((diff).T)*u.T*D*u*((diff).T)))
    print(errorL2)

    errorH1 = np.sqrt(
        np.abs(np.sum((diff.T*((dxu.T)*D*dxu)*diff) + (diff.T*((dyu.T)*D*dyu)*diff))))
    print(errorH1)
    print("-------------\n")
    return x, y, sol, vu, errorL2, errorH1


def solve_1():

    # Compute error
    edge_length = np.array([0.1, 0.05, 0.02, 0.01])
    aires = edge_length * edge_length
    L2e = []
    H1e = []
    for aire in aires:
        print("Solving for aire: {}".format(aire))
        # Construction du maillage
        m = triangle.triangulate(D, 'Dpa{}'.format(aire))
        _, _, _, _, errorL2, errorH1 = solve_dirichlet(m, order=2)
        L2e.append(errorL2)
        H1e.append(errorH1)
    plt.loglog(edge_length, edge_length*edge_length)
    plt.loglog(edge_length, edge_length*edge_length*edge_length)
    plt.loglog((edge_length), (L2e), '+r-')
    plt.loglog((edge_length), (H1e), '+b-')
    plt.show()


# solve_1()
